# Commit Lister

This is a GraphQL test app.
It is hooked to a GitLabAPI and fetches all commits in a current project.

NodeJS app is served on Heroku - https://commit-lister.herokuapp.com

To view the frontend app go to - https://georgeshevtsov.gitlab.io/commit-lister/

To start the project first install dependancies
```sh
$ npm i
```

To start the server. It be hosted on localhost:4000

```sh
$ npm run start
```

To start the frontend. It be hosted on localhost:3000
```sh
$ npm run start:ui
```