const express = require('express');
const { ApolloServer } = require('apollo-server-express');
const cors = require('cors');
const gql = require('graphql-tag');

const GitLabApi = require('../dataSources/gitlab');
const resolvers = require('../resolvers');

const schema = gql`
    type Query {
        commits: [Commit]
        commit(id: ID!): Commit
    }
    type Commit {
        id: ID
        type: String
        author_name: String
        author_email: String
        message: String
    }
`;

const app = express();
app.use(cors());

const server = new ApolloServer({
    typeDefs: schema,
    dataSources: () => ({
        GitLabAPI: new GitLabApi
    }),
    resolvers
});

server.applyMiddleware({ app, path: '/' });

const port = process.env.PORT || 4000;
app.listen(port);
console.log('Running a GraphQL API Server');