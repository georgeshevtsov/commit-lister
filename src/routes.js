import React from 'react';
import { 
    BrowserRouter as Router,
    Route, 
    Switch
} from 'react-router-dom';
import CommitsGrid from './components/CommitGrid';
import Commit from './components/Commit';

const Routes = () => {
    return (
        <Router basename={'/commit-lister'}>
            <Switch>
                <Route exact path={'/'} component={CommitsGrid} />
                <Route exact path={'/:id'} component={Commit}/>
            </Switch>
        </Router>
    );
}
export default Routes;