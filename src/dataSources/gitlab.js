const { RESTDataSource } = require('apollo-datasource-rest');

class GitLabAPI extends RESTDataSource {
    constructor() {
        super();
        this.baseURL = 'https://gitlab.com/api/v4';
    }
    async getAllCommits() {
        const response = await this.get(`projects/10926587/repository/commits`);
        return response;
    }
    async getCommitById({ commitId }) {
        const response = await this.getAllCommits();
        return response.find(commit => commit.id === commitId);
    }
}
module.exports = GitLabAPI;