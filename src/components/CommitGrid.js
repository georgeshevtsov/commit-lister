import React from 'react';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import { Link } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Loader from './Loader';

const styles = {
    gridContainer: {
        height: '100%'
    },
    button: {
        textDecoration: 'none'
    }
}

export const GET_COMMITS = gql`
    query GetCommits {
        commits {
            id
            author_name
            author_email
            message
        }
    }
`;

const CommitsGrid = (props) => {
    const { classes } = props;
    return (
        <div className={classes.gridContainer}>
            <Query 
                query={GET_COMMITS}
                displayName={'CommitsGrid'}
            >
                {
                    ({loading, data}) => {
                        return loading ? <Loader/> : (
                            <Paper>
                                <Table>
                                    <TableHead> 
                                        <TableRow>
                                            <TableCell>Author name</TableCell>
                                            <TableCell>Author email</TableCell>
                                            <TableCell>Message</TableCell>
                                            <TableCell></TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {data.commits.map(commit => (
                                                <TableRow key={commit.id}>
                                                    <TableCell>{commit.author_name}</TableCell>
                                                    <TableCell>{commit.author_email}</TableCell>
                                                    <TableCell>{commit.message}</TableCell>
                                                    <TableCell>
                                                    <Link className={classes.button} to={`/${commit.id}`}>
                                                        <Button
                                                            variant={'contained'} 
                                                            color={"primary"}
                                                        >
                                                            View
                                                        </Button>
                                                    </Link>
                                                    
                                                    </TableCell>
                                                </TableRow>
                                        ))}
                                    </TableBody>
                                </Table>
                            </Paper>
                        )
                    } 
                }
            </Query>
        </div>
    );   
}

export default withStyles(styles)(CommitsGrid);