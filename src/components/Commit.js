import React from 'react';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Loader from './Loader';
import { withStyles } from '@material-ui/core/styles';
import cx from 'classnames';

const GET_COMMIT = gql`
    query GetCommit($id: ID!)  {
        commit(id: $id) {
            id
            author_name
            author_email
            message
        }
    }
`;

const styles = {
    commitContainer: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%'
    },
    commitList: {
        listStyleType: 'none',
        padding: '0 '
    },
    commitListItem: {
        display: 'flex',
        justifyContent: 'space-between',
        padding: '8px 0'
    },
    listText: {
        margin: 0,
        fontSize: '16px'
    },
    listTextTitle: {
        fontWeight: '600'
    },
    listTextValue: {
        textAlign: 'right'
    },
    card: {
        width: '700px'
    }
}
const keyNormalizer = {
    author_name: 'name',
    author_email: 'email',
    message: 'message'
}

const CommitListItem = (props) => {
    const { classes, title, value } = props;
    return (
        <li className={classes.commitListItem}>
            <p className={cx(classes.listText, classes.listTextTitle)}>{title}:</p>  
            <p className={cx(classes.listText, classes.listTextValue)}>{value}</p>
        </li>
    )
}

const CommitList = (props) => {
    const { list, classes } = props;
    const componentsList = [];
    for (let key in list) {
        if(key in keyNormalizer) {
            componentsList.push(
                <CommitListItem classes={classes} key={key} title={keyNormalizer[key]} value={list[key]} />
            )
        }
    }
    return (
        <ul className={classes.commitList}>
            {componentsList}
        </ul>
    );
}

const Commit = (props) => {
    const { match, classes } = props;
    return (
        <div className={classes.commitContainer}>
            <Query
                query={GET_COMMIT}
                variables={{ id: match.params.id}}
                displayName={'Commit'}
            >
                {
                    ({ loading, data }) => {
                        const { commit } = data;
                        return loading ? <Loader/> : (
                            <Card className={classes.card}>
                                <CardHeader title={'Commit Details'}/>
                                <CardContent>
                                    <CommitList classes={classes} list={commit}/>
                                </CardContent>
                            </Card>
                        )
                    }
                }
            </Query>
        </div>
    )   
}
export default withStyles(styles)(withRouter(Commit));