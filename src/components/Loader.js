import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';

const styles = {
    loader: {
        width: '100%',
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    }
}
const Loader = (props) => {
    const { classes } = props;
    return (
        <div className={classes.loader}>
            <CircularProgress size={48} min={60} max={80} />
        </div>
    )
}
export default withStyles(styles)(Loader)