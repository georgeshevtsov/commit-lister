import ApolloClient from 'apollo-boost';
const client = new ApolloClient({
    uri: 'https://commit-lister.herokuapp.com'

});
export default client;