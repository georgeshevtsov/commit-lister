import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Routes from './routes';
import { ApolloProvider } from 'react-apollo';
import client from './apollo';

ReactDOM.render(
    <ApolloProvider client={client}>
        <Routes />
    </ApolloProvider>
    , document.getElementById('root')
);
