module.exports = {
    Query: {
        commits: async (_, __, { dataSources }) => {
            return dataSources.GitLabAPI.getAllCommits()
        },
        commit: async (_, { id }, { dataSources }) => {
            return dataSources.GitLabAPI.getCommitById({ commitId: id})
        }
    }
}